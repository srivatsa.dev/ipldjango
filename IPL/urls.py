from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('1', views.question_1, name='Q1'),
    path('2', views.question_2, name='Q2'),
    path('3', views.question_3, name='Q3'),
    path('4', views.question_4, name='Q4'),
    path('5', views.question_5, name='Q5'),
]
