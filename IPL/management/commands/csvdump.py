import mysql.connector
from mysql.connector import Error
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = 'Loads the csv file into mysql database'

    def handle(self, *args, **kwargs):
        try:
            mydb = mysql.connector.connect(host='52.14.28.173',
                                           database='ipl',
                                           user='dbuser',
                                           password='dbuser')
            mycursor = mydb.cursor()

            query_to_transfer_matches_to_mysql = "LOAD DATA LOCAL INFILE " \
                                                 "'/home/dell/PycharmProjects/IPL_Django/IPL/matches.csv' " \
                                                 "INTO TABLE ipl.dataproject_matches FIELDS TERMINATED BY ','" \
                                                 "  IGNORE 1 LINES;"
            mycursor.execute(query_to_transfer_matches_to_mysql)

            query_to_transfer_deliveries_to_mysql = "LOAD DATA LOCAL INFILE '/home/dell/PycharmProjects/IPL_Django" \
                                                    "/IPL/deliveries.csv' INTO TABLE ipl.dataproject_deliveries " \
                                                    "FIELDS TERMINATED BY ','  IGNORE 1 LINES(match_id,inning," \
                                                    "batting_team,bowling_team,over,ball,batsman,non_striker,bowler," \
                                                    "is_super_over,wide_runs,bye_runs,legbye_runs,noball_runs," \
                                                    "penalty_runs,batsman_runs,extra_runs,total_runs,player_dismissed" \
                                                    ",dismissal_kind,fielder);"

            mycursor.execute(query_to_transfer_deliveries_to_mysql)

            mycursor.close()
            mydb.close()
        except Error as e:
            print(e)

        finally:
            mydb.close()
