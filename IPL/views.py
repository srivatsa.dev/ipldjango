from django.http import HttpResponse
from django.conf import settings
from IPL.models import Matches, Deliveries
from django.core.cache.backends.base import DEFAULT_TIMEOUT
from django.views.decorators.cache import cache_page
from django.db.models import Count, Sum, Avg
from django.shortcuts import render

CACHE_TTL = getattr(settings, 'CACHE_TTL', DEFAULT_TIMEOUT)


@cache_page(CACHE_TTL)
def index(request):
    return HttpResponse("ROUTE USAGE:\n/1 - Plot the number of matches played per year of all the years in IPL.\n"
                        "/2 - Plot a stacked bar chart of matches won of all teams over all the years of IPL.\n"
                        "/3 - For the year 2016 plot the extra runs conceded per team.\n"
                        "/4 - For the year 2015 plot the top economical bowlers.\n"
                        "/5 - For the year 2016 plot the players with the highest caught dismissals")


@cache_page(CACHE_TTL)
def question_1(request):
    no_of_matches_per_season = Matches.objects.values('season').annotate(no_of_matches=Count('season'))
    season = []
    matches_won = []

    for items in no_of_matches_per_season:
        season.append(items['season'])
        matches_won.append(items['no_of_matches'])

    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Matches Played in Each IPL Season'},
        'xAxis': {'categories': season},
        'yAxis': {'title': {'text': 'No of Matches'}},
        'series': [{'name': 'Seasons', 'data': matches_won, 'color': 'blue'}]
    }

    return render(request, '../templates/graph_plot.html', {'chart': chart})


@cache_page(CACHE_TTL)
def question_2(request):
    wins_per_season = Matches.objects.exclude(winner__isnull=True)\
        .exclude(winner__exact='').values('winner', 'season').annotate(wins=Count('winner')).order_by('season')
    years = [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017]
    winning_per_year = {}
    for i in list(wins_per_season):
        if i['winner'] not in winning_per_year:
            winning_per_year[i['winner']] = [0] * 10
            winning_per_year[i['winner']][years.index(int(i['season']))] = i['wins']
        else:
            winning_per_year[i['winner']][years.index(int(i['season']))] = i['wins']

    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Matches Won by Each Team in each year'},
        'xAxis': {'categories': [2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017]},
        'yAxis': {'title': {'text': 'Matches Won'}},
        'legend': {'reversed': 'true'},
        'plotOptions': {'series': {'stacking': 'normal'}},
        'series': [{'name': name, 'data': value} for name, value in winning_per_year.items()]
    }
    return render(request, '../templates/graph_plot.html', {'chart': chart})


@cache_page(CACHE_TTL)
def question_3(request):
    matches_result = Matches.objects.filter(season= '2016')
    extra_runs_conceded = Deliveries.objects.filter(match_id__in=matches_result).only("bowling_team").values(
        "bowling_team").annotate(runs=Sum('extra_runs'))

    extra_runs_with_team_dict = {}
    for data in extra_runs_conceded:
        extra_runs_with_team_dict.update({data['bowling_team']: data['runs']})

    teams = []
    runs = []
    for key in sorted(extra_runs_with_team_dict.keys(), key=extra_runs_with_team_dict.get, reverse=True):
        teams.append(key)
        runs.append(extra_runs_with_team_dict[key])

    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Extra Runs Conceded by Each Team in 2016'},
        'xAxis': {'categories': teams},
        'yAxis': {'title': {'text': 'Extra Runs'}},
        'series': [{'name': 'Teams', 'data': runs, 'color': 'blue'}]
    }
    return render(request, '../templates/graph_plot.html', {'chart': chart})


@cache_page(CACHE_TTL)
def question_4(request):
    matches_result = Matches.objects.only("season", "id").filter(season='2015')
    economy_data = Deliveries.objects.filter(
        match_id__in=matches_result).values("bowler").annotate(avg=Avg("total_runs") * 6).order_by('avg')[:10]
    bowlers = []
    economy = []
    for items in economy_data:
        bowlers.append([items['bowler']])
        economy.append(items['avg'])

    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Top economical bowlers of 2015'},
        'xAxis': {'categories': bowlers},
        'yAxis': {'title': {'text': 'Economies'}},
        'series': [{'name': 'Players', 'data': economy, 'color': 'blue'}]
    }
    return render(request, '../templates/graph_plot.html', {'chart': chart})


@cache_page(CACHE_TTL)
def question_5(request):
    matches_result = Matches.objects.only("season", "id").filter(season='2016')
    caught_dismissal_data = Deliveries.objects.values('player_dismissed').annotate(
        caught_dismissals=Count('player_dismissed')).filter(dismissal_kind='caught').filter(
        match_id__in=matches_result).values('player_dismissed',
                                            'caught_dismissals').order_by('-caught_dismissals')[:10]
    players = []
    dtype = []
    for items in caught_dismissal_data:
        players.append([items['player_dismissed']])
        dtype.append(items['caught_dismissals'])
    chart = {
        'chart': {'type': 'column'},
        'title': {'text': 'Most batsmen with caught dismissals in IPL - 2016'},
        'xAxis': {'categories': players},
        'yAxis': {'title': {'text': 'Caught Dismissals'}},
        'series': [{'name': 'Players', 'data': dtype, 'color': 'blue'}]
    }
    return render(request, '../templates/graph_plot.html', {'chart': chart})
